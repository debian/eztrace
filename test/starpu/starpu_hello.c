#include <starpu.h>

void cpu_func(void *buffers[], void *cl_arg) {
  printf("Hello world\n");
}

struct starpu_codelet cl = {
  .cpu_funcs = { cpu_func },
  .nbuffers = 0
};

int main(int argc, char **argv) {
  /* initialize StarPU */
  starpu_init(NULL);
  struct starpu_task *task = starpu_task_create();
  task->cl = &cl; /* Pointer to the codelet defined above */

  /* starpu_task_submit will be a blocking call. If unset,
     starpu_task_wait() needs to be called after submitting the task. */
  task->synchronous = 1;
  /* submit the task to StarPU */
  starpu_task_submit(task);
  /* terminate StarPU */
  starpu_shutdown();
  return 0;
}
