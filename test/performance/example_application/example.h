/* -*- c-file-style: "GNU" -*- */
/*
 * Copyright (C) CNRS, INRIA, Université Bordeaux 1, Télécom SudParis
 * See COPYING in top-level directory.
 */

#ifndef __EXAMPLE_H__
#define __EXAMPLE_H__

#define NITER_COMPUTE 100

int lib_instrumented_function(double *t, int size, int n);
int lib_function(double *t, int size, int n);

#endif	/* __EXAMPLE_H__ */
