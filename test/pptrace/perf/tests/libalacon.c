/* -*- c-file-style: "GNU" -*- */
/*
 * Copyright (C) CNRS, INRIA, Université Bordeaux 1, Télécom SudParis
 * See COPYING in top-level directory.
 *
 *
 * libalacon.c
 *
 *  Created on: 4 Aug. 2011
 *      Author: Charles Aulagnon <charles.aulagnon@inria.fr>
 */

#include "alacon.h"

int foo(int a, int b) {
  return (a + b);
}

int bar() {
  return 42;
}
