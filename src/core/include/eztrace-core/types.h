/* -*- c-file-style: "GNU" -*- */
/*
 * Copyright (C) CNRS, INRIA, Université Bordeaux 1, Télécom SudParis
 * See COPYING in top-level directory.
 */

#ifndef TYPES_H
#define TYPES_H

#include <stdint.h>

// without explicit distinguishing between architectures, this code is portable on both x86_64 and ARMv7
#if __WORDSIZE == 64
typedef uint64_t word_uint;
typedef int64_t word_int;
#define WORD_HEX_FORMAT "%lx"
#define WORD_DEC_FORMAT "%ld"
#else // __WORDSIZE != 64
typedef uint32_t word_uint;
typedef int32_t word_int;
#define WORD_HEX_FORMAT "%x"
#define WORD_DEC_FORMAT "%d"
#endif // __WORDSIZE == 64
#endif // TYPES_H
#if __arm__
#define ARM_INSTR_SIZE 32
#endif
