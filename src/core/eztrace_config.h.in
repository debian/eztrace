/*
 * Copyright (C) CNRS, INRIA, Université Bordeaux 1, Télécom SudParis
 * See COPYING in top-level directory.
 *
 *
 * config.h -- Configuration parameters
 *
 *  Created on: 02 juil. 2011
 *      Author: Damien Martin-Guillerez <damien.martin-guillerez@inria.fr>
 *
 */

#ifndef PPTRACE_CONFIG_H_
#define PPTRACE_CONFIG_H_

#include "eztrace-core/eztrace_macros.h"

#include <stdio.h>
#include <string.h>

#define EZTRACE_VERSION "@CMAKE_PROJECT_VERSION@"

#define OTF2_VERSION "@OTF2_VERSION@"
#define OTF2_MAJOR_VERSION @OTF2_MAJOR_VERSION@
#define OTF2_MINOR_VERSION @OTF2_MINOR_VERSION@

// enum PPTRACE_BINARY_TYPE {
//   PPTRACE_BINARY_TYPE_BFD, // Requires -lbfd
//   PPTRACE_BINARY_TYPE_ELF, // Requires -lelf
// };
// 
#define PPTRACE_ARCH_TYPE_INTEL 1 // What else?

enum PTRACE_TRACE_TYPE {
  PTRACE_TRACE_TYPE_LINUX,
  PTRACE_TRACE_TYPE_MACOSX, // Not Yet Implemented
  PTRACE_TRACE_TYPE_BSD,    // Not Yet Implemented
};

enum PPTRACE_ISIZE_TYPE {
  PPTRACE_ISIZE_TYPE_TRACE,
  PPTRACE_ISIZE_TYPE_OPCODE, // Requires -lopcodes and binary type bfd
};

// Configuration
 
// Syscall dependency

#define HAVE_LIBOPCODE @HAVE_LIBOPCODE@

#define HAVE_LIBBACKTRACE @HAVE_LIBBACKTRACE@

#define USE_GETTID @USE_GETTID@

#define DYNLIB_SUFFIX  "@DYNLIB_EXT@"
#define DYNLIB_EXT     "@DYNLIB_EXT@"

#define EZTRACE_LIB_DIR "@EZTRACE_LIB_DIR@"

#define HAVE_PRCTL_H @HAVE_PRCTL_H@
#ifndef  __PPTRACE_USE_PRCTL
#if HAVE_PRCTL_H
// This flag is set to use prctl for ptrace to allow child to trace its parent on hardened systems
// Set it if prtcl.h, the prtcl() function and the PR_SET_PTRACER constant exists
#define __PPTRACE_USE_PRCTL
#endif /* HAVE_PRCTL_H */
#endif // defined  __PPTRACE_USE_PRCTL

#ifndef __PPTRACE_ARCH_TYPE
#define __PPTRACE_ARCH_TYPE PPTRACE_ARCH_TYPE_INTEL
#endif // defined __PPTRACE_ARCH_TYPE

#ifndef __PPTRACE_TRACE_TYPE
#define __PPTRACE_TRACE_TYPE PTRACE_TRACE_TYPE_LINUX
#endif // defined __PPTRACE_TRACE_TYPE

#ifndef __PPTRACE_ISIZE_TYPE
#define __PPTRACE_ISIZE_TYPE PPTRACE_ISIZE_TYPE_TRACE
#endif // defined __PPTRACE_ISIZE_TYPE

// Obscure internal structures
#define PPTRACE_HIJACK_FUNCTION       pptrace_hijack_list
#define PPTRACE_BASENAME PPTRACE_HIJACK_FUNCTION
#define PPTRACE_SYMBOL_LIST(module_name) CONCAT(CONCAT(PPTRACE_BASENAME, _), module_name)
#define PPTRACE_SYMBOL_ALIAS(module_name) STRINGIFY(PPTRACE_SYMBOL_LIST(module_name))
#define PPTRACE_SYMBOL_EXTERNAL(module_name) CONCAT(_, PPTRACE_SYMBOL_LIST(module_name))
#define PPTRACE_SYMBOL_ALIAS_STRING(dest, module_name_str, N) snprintf(dest, N, "%s_%s", STRINGIFY(PPTRACE_BASENAME), module_name_str)


#define PPTRACE_CURRENT_SYMBOL_LIST PPTRACE_SYMBOL_LIST(CURRENT_MODULE)
#define PPTRACE_CURRENT_SYMBOL_ALIAS PPTRACE_SYMBOL_ALIAS(CURRENT_MODULE)
#define PPTRACE_CURRENT_SYMBOL_EXTERNAL PPTRACE_SYMBOL_EXTERNAL(CURRENT_MODULE)

__attribute__((unused)) static void _get_pptrace_symbol_external(char* dest,
								 unsigned buffer_size,
								 const char* module_name);

/* copy the pptrace external symbol string corresponding to module_name in buffer dest
 */
__attribute__((unused)) static void _get_pptrace_symbol_external(char* dest,
								 unsigned buffer_size,
								 const char* module_name) {
  const char* prefix = STRINGIFY(PPTRACE_SYMBOL_EXTERNAL());
  if (buffer_size > strlen(prefix) + strlen(module_name)) {
    sprintf(dest, "%s%s", prefix, module_name);
  } else {
    /* not enough space in the buffer */
    dest[0] = '\0';
  }
}

int eztrace_autostart_enabled();


#define IOTRACER_PATH     "@IOTRACER_PATH@"
#define IOTRACER_TRACE_PATH "iotracer_trace.txt"

#endif /* PPTRACE_CONFIG_H_ */
