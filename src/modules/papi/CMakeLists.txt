# Target : libeztrace-papi.so
add_library(eztrace-papi SHARED
  papi.c
  papi_ev_codes.h
)

target_include_directories(eztrace-papi
  PUBLIC
    ${LIBPAPI_INCLUDE_DIRS}
)

target_compile_options(eztrace-papi
  PRIVATE
    -Wall -Wextra -Wpedantic
    -Werror
)

target_link_libraries(eztrace-papi
  PUBLIC
    eztrace-core
    papi
  PRIVATE
    eztrace-lib
    Threads::Threads
    ${INTERCEPT_LIB}
)

install(TARGETS eztrace-papi
  LIBRARY DESTINATION lib
)
