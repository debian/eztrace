#ifndef IOTRACER_TO_OTF2
#define IOTRACER_TO_OTF2


#include <otf2/otf2.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <assert.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h> 

#include <inttypes.h> 


void convert_iotracer_log_to_otf2();

#endif /* IOTRACER_TO_OTF2 */

