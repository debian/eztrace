# Requirements
In order to run EZTrace, you need the following software:

  * CMake;

  * OTF2 (which can be downloaded here [https://www.vi-hps.org/projects/score-p](https://www.vi-hps.org/projects/score-p))


# Getting EZTrace

## Building from source
* You can get the latest stable release on EZTrace website:
  https://eztrace.gitlab.io/eztrace

* Current development version is available via GIT
  `git clone https://gitlab.com/eztrace/eztrace.git`

## Spack package

[Spack](https://github.com/spack/spack/) ships a package for building
EZTrace and its dependencies. To install EZTrace, run:

```
spack install eztrace
```

To build EZTrace with the `ompt` module (that automatically instruments
OpenMP applications), you need to install `llvm`:

```
spack install eztrace %llvm@12.0.0
```

## Docker container

[Several docker images](https://hub.docker.com/u/eztrace) contain
installed version of eztrace. Some images also contain installed
versions of [ViTE](https://gitlab.inria.fr/solverstack/vite/).

To use one of these docker images:
```
 docker run -v $PWD:/shared -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=$DISPLAY -h $HOSTNAME -v $HOME/.Xauthority:/root/.Xauthority -it eztrace/eztrace.mpich.vite:latest bash 
```


# Checking EZTrace installation

Once EZTrace is installed, you should have several executable including `eztrace`, `eztrace_avail`, or `eztrace.preload`.

To check if everything works fine, see the [Using EZTrace](using.md) page
