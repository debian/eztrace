eztrace (2.1.1-5) unstable; urgency=medium

  * watch: Use api to get tags.
    - Add more runpath lintian override.
  * control, rules: Enable ompt on riscv64 and loong64.

 -- Samuel Thibault <sthibault@debian.org>  Sat, 11 Jan 2025 02:28:03 +0100

eztrace (2.1.1-4) unstable; urgency=medium

  * tests/control: Also disable ompt tests on i386 and riscv64.

 -- Samuel Thibault <sthibault@debian.org>  Sun, 10 Nov 2024 19:41:06 +0100

eztrace (2.1.1-3) unstable; urgency=medium

  * control, rules: Disable ompt on armhf.

 -- Samuel Thibault <sthibault@debian.org>  Sun, 10 Nov 2024 10:30:33 +0100

eztrace (2.1.1-2) unstable; urgency=medium

  * control, rules: Disable ompt on i386 and riscv64, enable it on armhf.
  * rules: Avoid confusion between mpich and openmpi builds.
  * rules: Disable binding on the smoke test.

 -- Samuel Thibault <sthibault@debian.org>  Sun, 10 Nov 2024 03:14:44 +0100

eztrace (2.1.1-1) unstable; urgency=medium

  * New upstream release.
    - debian/patches/git-self-link: Upstreamed.
    - debian/patches/git-starpu-1.4: Upstreamed.
    - debian/patches/git-starpu-1.4-task_submit: Upstreamed.
    - debian/patches/git-eztrace_avail: Upstreamed.
    - debian/patches/git-starpu-install: Upstreamed.
    - debian/patches/implicit: Upstreamed.
  * debian/rules: Run all testsuites before reporting the failures
    (Closes: Bug#1067468)
  * debian/rules: Add mpi smoketest rules.
  * debian/control: Depend on pkgconf instead of pkg-config.

 -- Samuel Thibault <sthibault@debian.org>  Sun, 10 Nov 2024 02:21:32 +0100

eztrace (2.1-7) unstable; urgency=medium

  * patches/implicit: Fix build with qa=+bug-implicit-func
    (Closes: Bug#1066332)

 -- Samuel Thibault <sthibault@debian.org>  Wed, 13 Mar 2024 12:39:56 +0100

eztrace (2.1-6) unstable; urgency=medium

  * Revert adding ompt support on more archs.

 -- Samuel Thibault <sthibault@debian.org>  Sun, 15 Oct 2023 16:14:57 +0200

eztrace (2.1-5) unstable; urgency=medium

  * Add ompt support on more archs.

 -- Samuel Thibault <sthibault@debian.org>  Sun, 15 Oct 2023 15:32:16 +0200

eztrace (2.1-4) unstable; urgency=medium

  * tests/control: Drop libpapi-dev dependency, libstarpu-dev already has it
    as appropriate.

 -- Samuel Thibault <sthibault@debian.org>  Mon, 11 Sep 2023 18:34:53 +0200

eztrace (2.1-3) unstable; urgency=medium

  * tests/control: Fix starpu tests by installing gawk.

 -- Samuel Thibault <sthibault@debian.org>  Sun, 10 Sep 2023 17:35:32 +0200

eztrace (2.1-2) unstable; urgency=medium

  * control: Build-dep on starpu >= 1.4.1+dfsg-7 to get starpu job start/end
    traced.
  * patches/starpu-job: Drop patch, now that job start/end gets traced.

 -- Samuel Thibault <sthibault@debian.org>  Mon, 28 Aug 2023 12:10:28 +0200

eztrace (2.1-1) unstable; urgency=medium

  * New upstream release
    - control: Add libnetcdf-dev and libpnetcdf-dev dependencies.
    - patches/git-arm: Upstreamed.
    - patches/git-atomic: Upstreamed.
    - patches/git-eztpomp: Upstreamed.
    - patches/git-eztrace_cc_path: Upstreamed.
    - patches/git-eztrace_cc_x: Upstreamed.
    - patches/git-functions: Upstreamed.
    - patches/git-input: Upstreamed.
    - patches/git-libatomic: Upstreamed.
    - patches/git-manpage: Upstreamed.
    - patches/git-memory-noopt: Upstreamed.
    - patches/git-missing-tests: Upstreamed.
    - patches/git-mpi-init: Upstreamed.
    - patches/git-mpi-init-called: Upstreamed.
    - patches/git-mpi-types: Upstreamed.
    - patches/git-mpi_module_name: Upstreamed.
    - patches/git-no-install-cmake: Upstreamed.
    - patches/git-opari2: Upstreamed.
    - patches/git-openmp-nested-lock: Upstreamed.
    - patches/git-otf2: Upstreamed.
    - patches/git-otf2-3: Upstreamed.
    - patches/git-path_space: Upstreamed.
    - patches/git-pkg-config: Upstreamed.
    - patches/git-pkgconfig: Upstreamed.
    - patches/git-rename: Upstreamed.
    - patches/git-test-conditioned: Upstreamed.
    - patches/git-test-mpi-path: Upstreamed.
    - patches/git-test-openmp: Upstreamed.
    - patches/git-test-openmp2: Upstreamed.
    - patches/git-test-path: Upstreamed.
    - patches/git-test-paths: Upstreamed.
    - patches/git-test-preload-libs: Upstreamed.
    - patches/git-test-programs-path: Upstreamed.
    - patches/git-test-source: Upstreamed.
    - patches/git-test-verbose: Upstreamed.
    - patches/otf2-2.0.3: Upstreamed.
    - patches/starpu-1.3: Upstreamed.
    - patches/norpath: Refreshed.
    - patches/werror: Refreshed.
    - patches/git-starpu-1.4: Fix finding starpu-1.4.
    - patches/git-starpu-1.4-task_submit: Fix building against starpu 1.4.
    - patches/starpu-job: Drop some test for now.
    - patches/git-eztrace_avail: Fix finding eztrace_avail.
    - patches/git-self-link: Fix linking against libeztpomp.
    - patches/git-starpu-install: Actually install StarPU module.
    - libeztrace0.lintian-overrides: Update.
    - rules: Enable STARPU, NETCDF and PNETCDF (where applicable).
    - rules: Set STARPU_HOME to CURDIR.
    - rules: Update blhc ignore patterns.
    - tests/control: Enable compiler_instrumentation and starpu tests.

 -- Samuel Thibault <sthibault@debian.org>  Fri, 25 Aug 2023 15:03:30 +0200

eztrace (2.0+repack-12) unstable; urgency=medium

  * control: Add missing breaks+replaces between libeztrace0 and
    libeztrace-dev, missed when moving /usr/lib/*/*.so from the latter to the
    former (Closes: Bug#1034956)

 -- Samuel Thibault <sthibault@debian.org>  Thu, 27 Apr 2023 16:07:04 +0200

eztrace (2.0+repack-11) unstable; urgency=medium

  * control: Always depend on otf2, eztrace now always requires it actually.

 -- Samuel Thibault <sthibault@debian.org>  Tue, 20 Dec 2022 16:17:31 +0100

eztrace (2.0+repack-10) unstable; urgency=medium

  * control: Fix arch exclusion.

 -- Samuel Thibault <sthibault@debian.org>  Tue, 20 Dec 2022 13:25:22 +0100

eztrace (2.0+repack-9) unstable; urgency=medium

  * patches/otf2-2.0.3: Fix build with otf2 2.0.3.

 -- Samuel Thibault <sthibault@debian.org>  Tue, 20 Dec 2022 13:12:31 +0100

eztrace (2.0+repack-8) unstable; urgency=medium

  * control: Make Multi-Arch: foreign.
  * control: Drop otf2 support on archs which don't have the atomics required
    by otf2.

 -- Samuel Thibault <sthibault@debian.org>  Tue, 20 Dec 2022 12:59:58 +0100

eztrace (2.0+repack-7) unstable; urgency=medium

  * rules, tests/control: Pass --oversubscribe to openmpi to avoid failure on
    machines with only one processor.
  * rules: Make openmpi failure fatal again, now that pmix is fixed in
    unstable.
  * rules: Fix installing headers in multiarch path.

 -- Samuel Thibault <sthibault@debian.org>  Fri, 26 Aug 2022 14:11:50 +0200

eztrace (2.0+repack-6) unstable; urgency=medium

  * control: Only build-dep on clang when we'll build the ompt module.

 -- Samuel Thibault <sthibault@debian.org>  Thu, 25 Aug 2022 14:41:14 +0200

eztrace (2.0+repack-5) unstable; urgency=medium

  * patches/git-mpi-init-called: MPI: avoid multiple initialization.
  * patches/git-test-openmp2: Fix OpenMP tests build.
  * debian/rules: Can now use dh_auto_test again.
  * debian/rules: Reduce build time by only building&testing the targeted
    modules.
  * debian/rules: Update blhc ignore line.

 -- Samuel Thibault <sthibault@debian.org>  Thu, 25 Aug 2022 14:01:01 +0200

eztrace (2.0+repack-4) unstable; urgency=medium

  * patches/test-verbose: Use upstream commit.
  * patches/test-programs-path: Fix the path of test programs.
  * patches/test-preload-libs: Use upstream commit.
  * rules: Pass EZTRACE_TEST_OPTION=-p.
  * patches/*: Reflush patches in the upstream order.
  * patches/git-otf2: Include upstream otf2 detection fix.
  * rules: Make mpi fork-safe.
  * patches/git-test-source: Fix running test from source with make test.
  * rules: Print blhc false positive suppressions (Closes: Bug#101804)
  * patches/git-openmp-nested-lock: Fix nested locks.

 -- Samuel Thibault <sthibault@debian.org>  Thu, 25 Aug 2022 09:53:43 +0200

eztrace (2.0+repack-3) unstable; urgency=medium

  * rules: Actually run tests at package build time.
  * control: Add bw, gawk build-deps.
  * patches/test-preload-libs: Add just-built libeztrace-lib.so to LD_PRELOAD.
  * patches/memory-noopt: Disable optimizations to make sure calloc etc. are
    kept.
  * patches/test-verbose: Show the actual test output.
  * copyright: Fix catching pomp files.

 -- Samuel Thibault <sthibault@debian.org>  Wed, 24 Aug 2022 14:18:42 +0200

eztrace (2.0+repack-2) unstable; urgency=medium

  * debian/copyright: Fix copyright information for pomp-lib-dummy.
  * libeztrace0.lintian-overrides: Complete overrides.
  * rules: Make tests verbose.
  * tests/control: Explicit list of architectures just like in debian/control.

 -- Samuel Thibault <sthibault@debian.org>  Wed, 24 Aug 2022 12:16:34 +0200

eztrace (2.0+repack-1) unstable; urgency=medium

  * src/modules/omp/pomp-lib-dummy: Restore eztrace's libpomp.
  * debian/copyright: Don't filter out pomp-lib-dummy.
  * patches/no-libpomp: Drop.
  * debian/libeztrace0.install: Add usr/lib/*/libeztpomp.so.
  * tests/control: Add build-essential, otf2-tools, clang, bc, gawk dependency.
  * tests/control: Make tests output verbose.
  * tests/control: Clean binaries between tests.
  * tests/control: Allow some stderr, skip non-installable ompt and openmpi.
  * tests/control: Enable openmp test.
  * libeztrace0.lintian-overrides: Complete overrides.
  * control: Fix libeztrace0 short description.
  * patches/git-test-mpi-path: Make mpi path configurable.
  * tests/control: Configure mpi paths.
  * patches/git-mpi-init: Fix mpi module name hardcoding.
  * tests/control: Enable mpi tests.
  * patches/git-libatomic: Fix 8-byte atomic use on some archs.

 -- Samuel Thibault <sthibault@debian.org>  Tue, 23 Aug 2022 17:02:45 +0200

eztrace (2.0-3) unstable; urgency=medium

  * patches/git-opari2: Fix build against opari2.
  * patches/git-arm: Fix build on some arm archs.
  * patches/git-mpi-types: Fix build with some mpi implementations.
  * patches/git-atomic: Try to fix atomic operations on mips.
  * patches/git-test-openmp: Fix openmp test.
  * rules: Enable openmp module.
  * patches/test_utils: replace with upstream's git-test-path.
  * tests/control: Add autopkgtest.

 -- Samuel Thibault <sthibault@debian.org>  Tue, 23 Aug 2022 13:52:13 +0200

eztrace (2.0-2) unstable; urgency=medium

  * patches/werror: Disable -Werror.
  * debian/control, rules: Enable ompt only on archs which have libomp-dev.
  * debian/control: eztrace: Add missing perl:Depends dependencies.

 -- Samuel Thibault <sthibault@debian.org>  Mon, 22 Aug 2022 16:07:10 +0200

eztrace (2.0-1) unstable; urgency=medium

  * New upstream release
    - control: Build-dep on cmake.
    - patches/binutils-2.38.50: Merged.
    - patches/extlib: Drop, now useless.
    - patches/no-rpath: Drop, now useless.
    - patches/ptrace-arm: Drop, now useless.
    - patches/no-libpomp: Update.
    - patches/reproducibility: Update.
    - patches/static-bfd: Update.
    - patches/rename: Fix discovering Debian's otf2.
    - rules: Update to cmake.
    - control: Replace fxt-tools, libgtg-dev, liblitl-dev (>= 0.1.9~),
      litl-tools build-deps with libotf2-trace-dev, otf2-tools.
    - control: Add opari2 build-dep.
    - control: Add clang, libomp-dev build-deps.
    - rules: Add ompt build with clang.
    - patches/otf2-3: Fix build against otf2 3.0.
    - docs, eztrace.install, libeztrace-dev.install, libeztrace0.install:
      Update installed files.
    - patches/test_utils: Workaround path to test_utils.sh
    - patches/norpath: Disable rpath.
    - control: Drop libeztrace-dev dependency on liblitl-dev.
    - patches/pkg-config: Drop spurious compilation flags.
    - patches/mpi_module_name: Fix loading right mpi module during tests.
    - patches/test-ompt: Fix enabling only tests for which a module was built.
    - patches/git-input: Fix path for template files.
    - patches/git-no-install-cmake: Avoid installing cmake files.
    - libeztrace0.lintian-overrides: Ignore missing version in modules.
    - patches/git-pkgconfig: Fix pkg-config file installation path.
    - patches/git-manpage: Install manpages.
    - patches/git-functions: Fix installation of functions.pm
  * watch: Update regexp.
  * gbp.conf, copyright: Replace gbp filter with copyright Files-Excluded.

 -- Samuel Thibault <sthibault@debian.org>  Mon, 22 Aug 2022 14:25:30 +0200

eztrace (1.1-11-3) unstable; urgency=medium

  * control: Bump Standards-Version to 4.6.0 (no change)
  * patches/binutils-2.38.50: Fix build against binutils 2.38.50.
    (Closes: Bug#1013517)

 -- Samuel Thibault <sthibault@debian.org>  Fri, 24 Jun 2022 14:08:20 +0200

eztrace (1.1-11-2) unstable; urgency=medium

  * Upload to unstable

 -- Samuel Thibault <sthibault@debian.org>  Mon, 23 Aug 2021 02:01:46 +0200

eztrace (1.1-11-1) experimental; urgency=medium

  * New upstream release
    - patches/no-inst-source: Upstreamed.
    - patches/no-inst-source2: Upstreamed.
    - patches/starpu-macro: Upstreamed.
  * rules: Use $(CURDIR) instead of $(PWD).
  * control: Set Rules-Requires-Root to no.

 -- Samuel Thibault <sthibault@debian.org>  Thu, 05 Aug 2021 17:36:34 +0200

eztrace (1.1-10-2) unstable; urgency=medium

  * control: Update upstream website.
  * copyright: Update upstream website.
  * patches/starpu-macros: Fix build against newer StarPU releases.
  * eztrace.install: Install /usr/share/eztrace (notably for function.pm).
  * patches/reproducibility: Avoid leaking build path.
  * rules: Pass build path through an environment variable.

 -- Samuel Thibault <sthibault@debian.org>  Fri, 01 Jan 2021 17:14:15 +0100

eztrace (1.1-10-1) unstable; urgency=medium

  * New upstream release.
    - patches/non-linux: Upstreamed.
    - patches/binutils-2.34: Upstreamed.
    - patches/ptrace-arm: Disable for now, tinkered upstream.
    - patches/extlib: Refreshed.
  * patches/no-inst-source: Fix installation of spurious files.
  * patches/no-inst-source2: Really fix installation of spurious files.
  * watch: Update watch URL.

 -- Samuel Thibault <sthibault@debian.org>  Wed, 04 Nov 2020 12:58:32 +0100

eztrace (1.1-8-7) unstable; urgency=medium

  [ Samuel Thibault ]
  * control: Drop mention of mips.
  * watch: Generalize pattern.
  * upstream/metadata: Add upstream meta information.
  * libeztrace-dev.install: Install libeztrace.a (but not the .a versions of
    plugins).
  * not-installed: Note that we don't install .la files and .a versions of
    plugins. Note that we don't install the example like upstream does: we
    just ship all example/.
  * rules: Rely on pre-initialized dpkg-architecture variables.

  [ Debian Janitor ]
  * Use secure URI in Homepage field.
  * Bump debhelper from deprecated 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Drop unnecessary dependency on dh-autoreconf.
  * Drop unnecessary dh arguments: --parallel
  * Update standards version to 4.5.0, no changes needed.
  * Remove Section on eztrace that duplicates source.

 -- Samuel Thibault <sthibault@debian.org>  Sun, 01 Nov 2020 02:17:21 +0100

eztrace (1.1-8-6) unstable; urgency=medium

  * control: Bump Standards-Version to 4.4.0 (no changes).
  * patches/binutils-2.34: Fix build against binutils 2.34 (Closes: Bug#951883)

 -- Samuel Thibault <sthibault@debian.org>  Mon, 24 Feb 2020 22:29:56 +0100

eztrace (1.1-8-5) unstable; urgency=medium

  * control: Make sure we build against starpu-1.3.
  * patches/starpu-1.3: Complete.

 -- Samuel Thibault <sthibault@debian.org>  Thu, 01 Aug 2019 17:31:34 +0200

eztrace (1.1-8-4) unstable; urgency=medium

  * patches/starpu-1.3: Fix build with starpu-1.3.

 -- Samuel Thibault <sthibault@debian.org>  Thu, 01 Aug 2019 17:16:31 +0200

eztrace (1.1-8-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/copyright: Change Format URL to correct one
  * d/control: Set Vcs-* to salsa.debian.org

  [ Samuel Thibault ]
  * rules: Set includedir to multi-arch, to avoid eztrace_config.h conflict
    between archs.

 -- Samuel Thibault <sthibault@debian.org>  Tue, 01 Jan 2019 16:51:14 +0100

eztrace (1.1-8-2) unstable; urgency=medium

  * debian/patches/ptrace-arm: Disable ptrace on armhf, it seems to hang there
    (Closes: Bug#909659).

 -- Samuel Thibault <sthibault@debian.org>  Fri, 28 Sep 2018 11:08:02 +0200

eztrace (1.1-8-1) unstable; urgency=medium

  * New upstream release.
    - debian/patches/armel-ftbfs.patch: Upstreamed.
    - debian/patches/big-endian-fix: Upstreamed.
  * Bump Standards-Version to 4.2.0 (no changes).

 -- Samuel Thibault <sthibault@debian.org>  Sat, 15 Sep 2018 09:49:19 +0200

eztrace (1.1-7-4) unstable; urgency=medium

  [ Adrian Bunk ]
  * patches/armel-ftbfs.patch: Fix build on armel (Closes: Bug#881355)

  [ Samuel Thibault ]
  * patches/non-linux: Fix build on non-Linux and x32.

 -- Samuel Thibault <sthibault@debian.org>  Fri, 10 Nov 2017 19:32:55 +0100

eztrace (1.1-7-3) unstable; urgency=medium

  [ François Trahay ]
  * patches/big-endian-fix: Complete fix for hang on big-endian archs (Closes: Bug#880266)

 -- Samuel Thibault <sthibault@debian.org>  Tue, 07 Nov 2017 18:42:45 +0100

eztrace (1.1-7-2) unstable; urgency=medium

  [ François Trahay ]
  * patches/big-endian-fix: Fix hang on big-endian archs (Closes: Bug#880266)

 -- Samuel Thibault <sthibault@debian.org>  Mon, 06 Nov 2017 13:45:43 +0100

eztrace (1.1-7-1) unstable; urgency=medium

  * New upstream release.
    - patches/pie: Remove, merged upstream.
    - patches/starpu-1.2: Remove, merged upstream.
    - patches/no-libpomp: Refresh.
    - patches/static-bfd: Refresh.
    - patches/binutils_2.29: Remove, merged upstream.
    - control: Bump liblitl-dev dependency.
  * control: Bump binutils dependency appropriately.
  * control: Migrate priority to optional.
  * control: Bump Standards-Version to 4.1.1.

 -- Samuel Thibault <sthibault@debian.org>  Mon, 30 Oct 2017 17:01:25 +0100

eztrace (1.1-5-6) unstable; urgency=medium

  * patches/binutils_2.29: Fix build with binutils (>= 2.29) (Closes:
    Bug#871151).

 -- Samuel Thibault <sthibault@debian.org>  Sun, 22 Oct 2017 15:29:16 +0200

eztrace (1.1-5-5) unstable; urgency=medium

  * rules: Fix multiarch path to openmpi headers.
  * Use canonical anonscm vcs URL.

 -- Samuel Thibault <sthibault@debian.org>  Sun, 22 Jan 2017 20:40:01 +0100

eztrace (1.1-5-4) experimental; urgency=medium

  * Apply upstream fix for PIE
  * rules: re-enable PIE.

 -- Samuel Thibault <sthibault@debian.org>  Fri, 11 Nov 2016 18:08:16 +0100

eztrace (1.1-5-3) unstable; urgency=medium

  * rules: Fix passing other hardening flags.

 -- Samuel Thibault <sthibault@debian.org>  Wed, 26 Oct 2016 11:14:03 +0200

eztrace (1.1-5-2) unstable; urgency=medium

  * starpu-1.2: Fix build against starpu-1.2.
  * rules: Really disable pie.

 -- Samuel Thibault <sthibault@debian.org>  Tue, 25 Oct 2016 21:55:10 +0200

eztrace (1.1-5-1) unstable; urgency=medium

  * New upstream release.

 -- Samuel Thibault <sthibault@debian.org>  Thu, 13 Oct 2016 23:39:12 +0200

eztrace (1.1-3-1) unstable; urgency=medium

  * New upstream release.
    - series: drop pptrace_symbol and config-fortran, merged upstream.
  * patches/static-bfd: Fix linking statically with libopcodes.

 -- Samuel Thibault <sthibault@debian.org>  Fri, 15 Jul 2016 02:34:43 +0200

eztrace (1.1-2-2) unstable; urgency=medium

  * Drop hardcoded openmpi availability, thanks Mattia Rizzolo for the
    suggestion.
    - control: Use mpi-defaults-dev instead of using a hardcoded condition on
    mpi-defaults-dev
    - rules: Include /usr/share/mpi-default-dev/debian_defaults and use
    ARCH_DEFAULT_MPI_IMPL instead of hardcoding architectures.
  * Add mips64el and x32 arch to papi dependency (Closes: Bug#814782).
  * patches/config-fortran: Cherry-pick upstream patch to fix fortran issue in
    configure (Closes: Bug#820863).
  * control: Make libeztrace-dev depend on liblitl-dev.
  * libeztrace-dev: Install usr/include/ev_codes.h too.
  * control: Bump Standards-Version to 3.9.8 (no change)

 -- Samuel Thibault <sthibault@debian.org>  Mon, 23 May 2016 00:02:44 +0200

eztrace (1.1-2-1) unstable; urgency=medium

  * New upstream release.
  * rules: Clear.
  * patches/static-bfd: Fix multiarch path (Closes: Bug#807036).
  * patches/pptrace_symbol: Drop protected attribute, now useless and doesn't
  work with cuda.
  * control: Bump Standards-Version to 3.9.7 (no change).

 -- Samuel Thibault <sthibault@debian.org>  Fri, 05 Feb 2016 12:55:33 +0100

eztrace (1.1-1-1) unstable; urgency=medium

  * New upstream release (Closes: Bug#804838).
    - patches/x32: Remove, merged upstream.
    - patches/no-rpath: Refresh.

 -- Samuel Thibault <sthibault@debian.org>  Fri, 11 Dec 2015 14:14:37 +0100

eztrace (1.1-3) unstable; urgency=medium

  * control: Drop hardening-wrapper build dependency.
  * compat: Bump to 9.
  * rules:
    - Replace hardening-wrapper variables with dpkg-buildflags variables.
    - Disable parallelism in testsuite.
  * patches/static-bfd: Fix path to libbfd.a.

 -- Samuel Thibault <sthibault@debian.org>  Mon, 16 Nov 2015 14:13:50 +0100

eztrace (1.1-2) unstable; urgency=medium

  * Fix x32 detection.

 -- Samuel Thibault <sthibault@debian.org>  Thu, 15 Oct 2015 15:58:59 +0200

eztrace (1.1-1) unstable; urgency=medium

  [ Samuel Thibault ]
  * New upstream release (Closes: Bug#798753).
    - control: Add libstarpu-dev build-dep.
    - rules: Pass --with-starpu=/usr

  [ bartm ]
  * watch: generalize URL and mangle rc tag.

 -- Samuel Thibault <sthibault@debian.org>  Tue, 13 Oct 2015 16:45:46 +0200

eztrace (1.1~rc1-2) experimental; urgency=medium

  * patches/git-a19e5ed-nopptrace: Cherry-pick upstream fix to disable pptrace
    on unsupported systems.

 -- Samuel Thibault <sthibault@debian.org>  Wed, 13 May 2015 09:47:27 +0200

eztrace (1.1~rc1-1) experimental; urgency=medium

  * New upstream release
    - patches/{git-0cb79edc3411c0e04e411d7c8f60a6596632a4ea,
    git-8be2d52dfe036666a75160aa33531a52d5f2257a, git-ebe444a, libdir,
    no-armv7.patch: Remove patches, merged upstream.
    - patches/{no-libpomp, static-bfd}: Refresh.
    - control: Add pkg-config build-dep.
      Disable papi build-dep on some d-p archs.
    - rules: Remove strip hack, fixed upstream.

 -- Samuel Thibault <sthibault@debian.org>  Sun, 03 May 2015 18:00:47 +0200

eztrace (1.0.6-6) unstable; urgency=medium

  * Update architecture lists from dependencies.

 -- Samuel Thibault <sthibault@debian.org>  Fri, 28 Aug 2015 14:20:32 +0200

eztrace (1.0.6-5) unstable; urgency=medium

  * patches/git-1f988a88ea495623164c073d5bd99220b4b3fcde: Cherry-pick from
    upstream to fix gcc-5 build (Closes: Bug#777847).
  * watch: Generalize URL.

 -- Samuel Thibault <sthibault@debian.org>  Tue, 16 Jun 2015 23:46:06 +0200

eztrace (1.0.6-4) unstable; urgency=medium

  * patches/libdir: Fix plugin path search for the pptrace case
    (closes: Bug#775286).
  * rules: Do not strip modules for pptrace auto-load, otherwise they won't be
    loadable.

 -- Samuel Thibault <sthibault@debian.org>  Wed, 14 Jan 2015 14:52:50 +0100

eztrace (1.0.6-3) unstable; urgency=medium

  * patches/git-ebe444a: Cherry-pick from upstream to fix uninitialized value.

 -- Samuel Thibault <sthibault@debian.org>  Tue, 02 Dec 2014 22:56:57 +0100

eztrace (1.0.6-2) unstable; urgency=medium

  [ Peter Michael Green ]
  * Remove -march=armv7-a from CFLAGS (Closes: Bug#770793).

  [ Samuel Thibault ]
  * Only enable pptrace support (which needs armv7) on armhf.

 -- Samuel Thibault <sthibault@debian.org>  Mon, 24 Nov 2014 14:38:20 +0100

eztrace (1.0.6-1) unstable; urgency=medium

  * New upstream release.
  * patches/git-0cb79edc3411c0e04e411d7c8f60a6596632a4ea: Fix bogus
    parameters (Closes: Bug#760502).
  * patches/git-8be2d52dfe036666a75160aa33531a52d5f2257a: Fix licence
    advertised in documentation.
  * patches/pptrace, non_linux: Remove, merged upstream.
  * patches/check: Remove, fixed upstream.
  * Bump Standards-Version to 3.9.6 (no changes).

 -- Samuel Thibault <sthibault@debian.org>  Thu, 23 Oct 2014 15:56:45 +0200

eztrace (1.0.5-3) unstable; urgency=medium

  * Disable EZTRACE_SIGALRM on !linux.

 -- Samuel Thibault <sthibault@debian.org>  Thu, 11 Sep 2014 15:43:43 +0200

eztrace (1.0.5-2) unstable; urgency=medium

  * Fix disabling pptrace on !linux !amd64 !arm.

 -- Samuel Thibault <sthibault@debian.org>  Wed, 10 Sep 2014 18:05:24 +0200

eztrace (1.0.5-1) unstable; urgency=medium

  * New upstream release.
    - patches/fix_fortran_mpi_scatterv.patch: Remove, fixed upstream.
    - patches/Fix-fortan-communications-using-MPI_IN_PLACE-option.patch:
    Remove, fixed upstream.
    - patches/rpath: Remove, fixed upstream.
    - patches/static-bfd: Refresh.
  * rules: Fix calling testsuite.

 -- Samuel Thibault <sthibault@debian.org>  Wed, 10 Sep 2014 17:23:49 +0200

eztrace (1.0.1-2) unstable; urgency=medium

  * patches/fix_fortran_mpi_scatterv.patch: Patch from Bertrand Putigny to fix
    the MPIF_Scatterv call.
  * patches/Fix-fortan-communications-using-MPI_IN_PLACE-option.patch: Patch
    from Bertrand Putigny to fix fortran mpi communications using MPI_IN_PLACE.
  * rules: Add parallel build support.

 -- Samuel Thibault <sthibault@debian.org>  Wed, 10 Sep 2014 14:01:53 +0200

eztrace (1.0.1-1) unstable; urgency=medium

  * New upstream bugfix release.
  * control: Do not depend on openmpi on s390x (Closes: Bug#754624).
  * rules: Do not build against openmpi on s390x. Drop duplicate
  --with-mpi-include option. Run the testsuite against openmpi on other archs.

 -- Samuel Thibault <sthibault@debian.org>  Sun, 13 Jul 2014 16:29:46 +0200

eztrace (1.0-1) unstable; urgency=medium

  * New upstream release.
  * Add missing zlib1g-dev build-dependency.
  * debian/copyright: Update, eztrace is now CeCILL-B instead of GPL2.
  * debian/rules: Build both mpich and openmpi MPI modules.
  * debian/control: Build-Depend on both mpich and openmpi.

 -- Samuel Thibault <sthibault@debian.org>  Thu, 26 Jun 2014 09:47:38 +0200

eztrace (1.0~rc4-3-1) experimental; urgency=medium

  * New upstream half-release.
  * Disable pptrace on i386 too, as it is missing the MMAP_SYSCALLS
    definition. Enable it on arm, which now support it.

 -- Samuel Thibault <sthibault@debian.org>  Wed, 25 Jun 2014 13:45:12 +0200

eztrace (1.0~rc4-2-1) experimental; urgency=medium

  * New upstream half-release.

 -- Samuel Thibault <sthibault@debian.org>  Wed, 25 Jun 2014 11:08:49 +0200

eztrace (1.0~rc4-1) experimental; urgency=medium

  * New upstream release.
    - patches/bibliography: Remove patch, fixed upstream
  * control: Add missing binutils-dev and litl-tools build-deps.

 -- Samuel Thibault <sthibault@debian.org>  Tue, 24 Jun 2014 18:04:42 +0200

eztrace (1.0~rc3-1) experimental; urgency=medium

  * New upstream release.
    - patches/{libdir,link}: Remove patch, fixed upstream.
    - patches/bibliography: Fix building the documentation.
  * Do not install static libraries.

 -- Samuel Thibault <sthibault@debian.org>  Wed, 11 Jun 2014 09:13:23 +0200

eztrace (1.0~rc1-2) experimental; urgency=medium

  * Depend on libpapi-dev only on Linux archs, and not on s390x.

 -- Samuel Thibault <sthibault@debian.org>  Tue, 20 May 2014 18:46:40 +0200

eztrace (1.0~rc1-1) experimental; urgency=low

  * New upstream release.
    - patches/manpages, test-fix, mpi-fix, mpi3, pptrace-option,
    clang-ftbfs: Drop, integrated upstream.
    - control: Replace libfxt-dev build-depend with liblitl-dev.
    - rules: Replace fxt with litl.
  * patches/link: New patch, fix missing link againt liblitl.
  * patches/rpath: New patch, drop /usr/lib rpath.
  * control: Build-depend on libpapi-dev.

 -- Samuel Thibault <sthibault@debian.org>  Mon, 19 May 2014 23:43:05 +0200

eztrace (0.9.1-6) unstable; urgency=low

  [ Arthur Marble ]
  * Fix FTBFS with clang (Closes: #743007).
    - Fixed the conflicting types error in
      src/core/eztrace_convert_core.c: moved a function above before where it is
      called.

 -- Samuel Thibault <sthibault@debian.org>  Wed, 02 Apr 2014 17:14:00 +0200

eztrace (0.9.1-5) unstable; urgency=low

  * control: Replace binutils-dev build-dependency with libiberty-dev for
    libiberty.a
    (Closes: #730945).

 -- Samuel Thibault <sthibault@debian.org>  Mon, 02 Dec 2013 16:31:23 +0100

eztrace (0.9.1-4) unstable; urgency=low

  * Bump Standards-Version to 3.9.5 (no changes).
  * patches/static-bfd: Link against libbfd statically to avoid rebuilding on
    each and every binutils upload (Closes: #728084).

 -- Samuel Thibault <sthibault@debian.org>  Fri, 01 Nov 2013 16:05:36 +0100

eztrace (0.9.1-3) unstable; urgency=medium

  * patches/manpages: Import manpages from upstream trunk.
  * control: Make eztrace depend on the same version of libeztrace0.
  * patches/libdir: Fix autostart load by fixing multiarch libdir.

 -- Samuel Thibault <sthibault@debian.org>  Wed, 09 Oct 2013 19:01:27 +0200

eztrace (0.9.1-2) unstable; urgency=low

  * patches/pptrace-option: Add --without-pptrace option.
  * rules: Disable pptrace on non-linux-x86 ports.

 -- Samuel Thibault <sthibault@debian.org>  Fri, 06 Sep 2013 00:23:00 +0200

eztrace (0.9.1-1) unstable; urgency=low

  * New upstream release.
    - patches/parameter_parsing_crash: Drop, merged upstream.
    - docs: Add README.stats.

 -- Samuel Thibault <sthibault@debian.org>  Thu, 05 Sep 2013 17:33:34 +0200

eztrace (0.9-1) unstable; urgency=low

  * New upstream release.
    - patches/gtg: Remove, fixed upstream.
    - patches/extlib: Refresh.
    - control: switch to opari2.
  * Bump Standards-Version to 3.9.4 (no changes).
  * rules: Disable PIE hardening, it makes the static test module hang.
  * patches/test-fix: Fix test segfault.
  * patches/plugins-abi-version: Fix static test module too.
  * patches/mpi-fix: Fix list issue in MPI conversion.
  * patches/mpi3: Fix bindnow support when MPI3 is not supported.
  * control: add mpirun.
  * patches/no-libpomp: Do not build a libpomp, use the one from libpomp2-dev.

 -- Samuel Thibault <sthibault@debian.org>  Thu, 05 Sep 2013 16:30:05 +0200

eztrace (0.7-2-5) unstable; urgency=low

  * control: Make libeztrace-dev Multi-Arch: same.
  * patches/parameter_parsing_crash: Fix a parameter parsing crash.

 -- Samuel Thibault <sthibault@debian.org>  Thu, 27 Jun 2013 14:21:44 +0200

eztrace (0.7-2-4) unstable; urgency=low

  * patches/plugins-abi-version: Fix abi version in configure.ac instead of
    source code, as it is needed in several places.

 -- Samuel Thibault <sthibault@debian.org>  Sat, 30 Jun 2012 10:25:42 -0300

eztrace (0.7-2-3) unstable; urgency=low

  * patches/plugins-abi-version: Fix module loading without libeztrace-dev
    installed by making eztrace load with abi version, not just .so.
  * Only ship /usr/include/eztrace*

 -- Samuel Thibault <sthibault@debian.org>  Fri, 29 Jun 2012 13:16:49 -0300

eztrace (0.7-2-2) unstable; urgency=low

  * control: Move eztrace to section devel.

 -- Samuel Thibault <sthibault@debian.org>  Thu, 24 May 2012 01:22:08 +0200

eztrace (0.7-2-1) unstable; urgency=low

  * Initial release (Closes: 658116)

 -- Samuel Thibault <sthibault@debian.org>  Sun, 20 May 2012 21:29:16 +0200
